#define fan 6//ventilator horn evacuare
#define valve 8//valva gaz
#define spark 10//scanteie
#define error A1//eroare
#define gnd_fan 7//ventilator horn evacuare
#define gnd_valve 9//valva gaz
#define gnd_spark 11//scanteie
#define gnd_error A2//eroare
#define pin_horn 2//presiune horn
#define pin_flacara 3//detectie ionica
#define pin_gaz 4//presiune gaz
#define pin_apa 5//temperatura apa
#define STATE_0 0
#define STATE_1 1
#define STATE_2 2
#define STATE_3 3
#define STATE_4 4
#define STATE_5 5
#define STATE_6 6
#define STATE_7 7
#define STATE_1_2 8
#define STATE_5_2 9
#define DELAY_STATE_1 1000
#define DELAY_STATE_2 1000
#define DELAY_STATE_4 1000
#define DELAY_STATE_5 2000
#define DELAY_STATE_6 1000
#define DELAY_STATE_7 1000
#define DELAY_TEST 1000

void setup() {
  //outputs:
  pinMode(fan,OUTPUT);
  pinMode(valve,OUTPUT);
  pinMode(spark,OUTPUT);
  pinMode(error,OUTPUT);
  pinMode(gnd_fan,OUTPUT);
  pinMode(gnd_valve,OUTPUT);
  pinMode(gnd_spark,OUTPUT);
  pinMode(gnd_error,OUTPUT);
  //inputs:
  pinMode(pin_gaz,INPUT_PULLUP);
  pinMode(pin_apa,INPUT_PULLUP);
  pinMode(pin_horn,INPUT_PULLUP);
  pinMode(pin_flacara,INPUT_PULLUP);
  //set GND:
  digitalWrite(gnd_fan,LOW);
  digitalWrite(gnd_valve,LOW);
  digitalWrite(gnd_spark,LOW);
  digitalWrite(gnd_error,LOW);
  //test LEDs:
  digitalWrite(fan,HIGH);
  digitalWrite(valve,HIGH);
  digitalWrite(spark,HIGH);
  digitalWrite(error,HIGH);
  delay(DELAY_TEST);
  digitalWrite(fan,LOW);
  digitalWrite(valve,LOW);
  digitalWrite(spark,LOW);
  digitalWrite(error,LOW);
  //interuperi:
  attachInterrupt(digitalPinToInterrupt(pin_horn),isr_horn,CHANGE);
  attachInterrupt(digitalPinToInterrupt(pin_flacara),isr_flacara,CHANGE);
  //seriala:
  Serial.begin(9600);
}

volatile bool steag_horn, steag_flacara;

void loop() {
  static byte state = 0;//starea atuomatului
  static byte count = 0;//contor de erori
  static byte prev_count = 1000;
  static bool fire = false;
  bool gaz = digitalRead(pin_gaz);
  bool apa = digitalRead(pin_apa);
  bool horn = digitalRead(pin_horn) && steag_horn;//atentie, intreruperile pot fi folosite in urmatoarele conditii:
  bool flacara = digitalRead(pin_flacara) && steag_flacara;//pulsurile trebuie sa fie cuprinse intre 1-2 secunde
  Serial.print(steag_flacara);
  switch (state) {
    case STATE_0:
      if(fire && !flacara) ++count;
      digitalWrite(error,LOW);//nu avem eroare daca a ajuns in start
      if(count >= 3) state = STATE_7;
      else if(!gaz || !apa) state = STATE_2;
      else if(flacara && fire) state = STATE_3;
      else if(flacara) state = STATE_0, digitalWrite(error,HIGH);//daca apare flacara fara gaz e grav
      else state = STATE_1;
      break;
    case STATE_1:
      digitalWrite(fan,HIGH);
      delay(DELAY_STATE_1);
      state = STATE_1_2;
      break;
    case STATE_1_2:
      if(horn) state = STATE_5;
      else state = STATE_4;
      break;
    case STATE_2:
      digitalWrite(valve,LOW);//se opreste temporar incalzirea
      digitalWrite(fan,LOW);//scanteia oricum nu este activa
      delay(DELAY_STATE_2);
      fire = false;
      state = STATE_0;
      break;
    case STATE_3:
      digitalWrite(fan,HIGH);
      if(horn) state = STATE_0;
      else state = STATE_4;
      break;
    case STATE_4:
      digitalWrite(valve,LOW);
      digitalWrite(fan,LOW);//nu este imperios
      ++count;
      delay(DELAY_STATE_4);
      fire = false;
      state = STATE_0;
      break;
    case STATE_5:
      fire = true;
      digitalWrite(valve,HIGH);
      digitalWrite(spark,HIGH);
      delay(DELAY_STATE_5);//un glitch pentru scanteie
      digitalWrite(spark,LOW);
      state = STATE_5_2;
      break;
    case STATE_5_2:
      if(flacara) state = STATE_0;
      else state = STATE_6;
      break;
    case STATE_6:
      digitalWrite(valve,LOW);
      digitalWrite(fan,LOW);//nu este imperios
      delay(DELAY_STATE_6);
      ++count;
      fire = false;
      state = STATE_0;
      break;
    case STATE_7:
      digitalWrite(valve,LOW);
      digitalWrite(fan,LOW);
      digitalWrite(error,HIGH);
      delay(DELAY_STATE_7);
      digitalWrite(error,LOW);
      delay(DELAY_STATE_7);
      state = STATE_7;
      //nu mai este necesar un break fiind ultima stare
  }
}

volatile unsigned long debounce_time_us = 1000000;//constanta de debounce
volatile unsigned long last_value;
volatile bool flag_first_horn = true, flag_first_flacara = true;

void isr_horn(){
  if(micros() - last_value >= debounce_time_us){//debouncing, ajuta la testarea manuala
    if(flag_first_horn){
      flag_first_horn = false;
      if(digitalRead(pin_horn)) steag_horn = true;
      else steag_horn = false;
    } else {
      steag_horn = !steag_horn;
    }
  }
}

void isr_flacara(){//in practica se vor comenta liniile pentru debouncing
  if(micros() - last_value >= debounce_time_us){//debouncing-ul ajuta la testarea manuala
    if(flag_first_flacara){
      flag_first_flacara = false;
      if(digitalRead(pin_flacara)) steag_flacara = true;
      else steag_flacara = false;
    } else {
      steag_flacara = !steag_flacara;
    }
  }
}
